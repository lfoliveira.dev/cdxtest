import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';

import AppNavigator from './navigation/AppNavigator';

export default class App extends Component {


    
  render() {
    return(
        <View style={{ flex: 1, }}>
        <AppNavigator />
        </View>
    )
  }

}

