import React, { Component } from 'react';
import {
  Switch,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Button
} from 'react-native';
import Constants from 'expo-constants'
import * as Animatable from 'react-native-animatable'
import Collapsible from 'react-native-collapsible'
import Accordion from 'react-native-collapsible/Accordion'
import { Ionicons, FontAwesome } from '@expo/vector-icons'


import notification from '../../assets/exclamation.png'
import correcto from '../../assets/correct.png'
import up from '../../assets/up-arrow.png'
import relogio from '../../assets/clock.png'


const CONTENT = [
  {
    title: 'Pendente',
   
  },


];


export default class CardsReal extends Component {
  constructor() {
    super()

  
  this.state = {
    activeSections: [],
    collapsed: true,
    multipleSelect: false,
  };

}

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections,
    });
  };

  renderHeader = (section, _, isActive) => {
    return (

      <View>
      <Animatable.View
        duration={200}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor">


      <View style={{ flexDirection: 'row', }}>
      <Image source={notification} style={{ height: 25, width: 25, }} />
        <Text style={styles.headerText}>{section.title}</Text>
        <Text style={{ marginLeft: 70 }}>06 MAI 2019</Text>
        <Text style={{ marginLeft: 10 }}>14:25</Text>
      </View>
      
      <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
        <Text style={{ paddingTop: 5 }}>Valor a ser pago:</Text>
        <Animatable.Text 
        animation={isActive ? 'bounceIn' : undefined}
        style={{ marginTop: 5, marginLeft: 55, fontWeight: 'bold', fontSize: 15 }}
        >
        R$ 22.539,52</Animatable.Text>
        
      </View>
      <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
        <Text style={{ paddingTop: 5 }}>Valor a receber:</Text>
        <Text style={{ marginTop: 5, marginLeft: 85 }}> 1.0000 BTC</Text>
      </View>
      </Animatable.View>      
      </View>

    );
  };

  renderContent(section, _, isActive) {
    return (
      <Animatable.View
        duration={400}
        style={[styles.content, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
        <View style={{ marginVertical: 20, }}>
        <Animatable.Text animation={isActive ? 'bounceIn' : undefined}>
          Conta: Atar
        </Animatable.Text>

        <Text style={{ paddingTop: 10 }}>
          Minha Carteira: 
        </Text>
        <Text>
            dlhduh89173ehsjadaaishaushauysgaysga98491k
        </Text>


        <View style={{ flexDirection: 'row', marginLeft: 150, paddingTop: 40  }}>
        <TouchableOpacity>
        <Image 
        source={up}
        style={{ height: 30, width: 30, }}
        />
        </TouchableOpacity>
        <View style={{ marginLeft: 90 }}>
        <TouchableOpacity style={{ alignItems: 'center', borderRadius: 20, backgroundColor: '#00BFFF', width: 60, height: 20, }}>
          <Text style={{ fontSize: 15, color: '#fff' }}>Pagar</Text>
        </TouchableOpacity>
        </View>
        </View>

        </View>

      </Animatable.View>
    );
  }

  render() {
    const { multipleSelect, activeSections } = this.state;

    return (
      <ScrollView contentContainerStyle={{ paddingTop: 10, }}>
      <View style={styles.container}>
          <Accordion
            activeSections={activeSections}
            sections={CONTENT}
            touchableComponent={TouchableOpacity}
            expandMultiple={multipleSelect}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
          
          <View style={styles.content}>
          <View style={{ flexDirection: 'row', }}>
          <Image source={notification} style={{ height: 25, width: 25, }} />
          <Text style={styles.headerText}>Pendente</Text>
          <Text style={{ marginLeft: 70 }}>06 MAI 2019</Text>
          <Text style={{ marginLeft: 10 }}>11:12</Text>
          </View>

          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a ser pago:</Text>
          <Text 
          style={{ marginTop: 5, marginLeft: 55, fontWeight: 'bold', fontSize: 15 }}
          >
          R$ 16.230,00</Text>

          </View>
          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a receber:</Text>
          <Text style={{ marginTop: 5, marginLeft: 85 }}> 0.7195 BTC</Text>
          </View>
          </View>



          <View style={styles.content}>
          <View style={{ flexDirection: 'row', }}>
          <Image source={correcto} style={{ height: 25, width: 25, }} />
          <Text style={styles.headerTextEfetuado}>Efetuado</Text>
          <Text style={{ marginLeft: 70 }}>06 MAI 2019</Text>
          <Text style={{ marginLeft: 10 }}>09:18</Text>
          </View>

          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a ser pago:</Text>
          <Text 
          style={{ marginTop: 5, marginLeft: 65, fontWeight: 'bold', fontSize: 15 }}
          >
          R$ 9.540,60</Text>

          </View>
          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a receber:</Text>
          <Text style={{ marginTop: 5, marginLeft: 70, fontSize: 17 }}> 0.4234 BTC</Text>
          </View>
          </View>

          <View style={styles.content}>
          <View style={{ flexDirection: 'row', }}>
          <Image source={correcto} style={{ height: 25, width: 25, }} />
          <Text style={styles.headerTextEfetuado}>Efetuado</Text>
          <Text style={{ marginLeft: 70 }}>06 MAI 2019</Text>
          <Text style={{ marginLeft: 10 }}>08:00</Text>
          </View>

          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a ser pago:</Text>
          <Text 
          style={{ marginTop: 5, marginLeft: 65, fontWeight: 'bold', fontSize: 15 }}
          >
          R$ 4.250,00</Text>

          </View>
          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a receber:</Text>
          <Text style={{ marginTop: 5, marginLeft: 70, fontSize: 17 }}> 0.1883 BTC</Text>
          </View>
          </View>


          <View style={styles.contentExpirado}>
          <View style={{ flexDirection: 'row', }}>
          <Image source={relogio} style={{ height: 25, width: 25, }} />
          <Text style={styles.headerTextExpirado}>Expirado</Text>
          <Text style={{ marginLeft: 70 }}>06 MAI 2019</Text>
          <Text style={{ marginLeft: 10 }}>14:25</Text>
          </View>

          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a ser pago:</Text>
          <Text 
          style={{ marginTop: 5, marginLeft: 25, fontWeight: 'bold', fontSize: 15 }}
          >
          R$ 21.725,60 BLR</Text>

          </View>
          <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
          <Text style={{ paddingTop: 5 }}>Valor a receber:</Text>
          <Text style={{ marginTop: 5, marginLeft: 85 }}> 1.0000 BTC</Text>
          </View>
          </View>
        
        </View>
        </ScrollView>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f2f2',
    paddingTop: Constants.statusBarHeight,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
    borderRadius: 20,
  },
  headerText: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#FF0000'
  },
  headerTextEfetuado: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#000000'
  },
  headerTextExpirado: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#000000'
  },
  content: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    marginVertical: 10,
    borderRadius: 25
  },
  contentExpirado: {
    padding: 20,
    backgroundColor: '#E8E8E8E8',
    marginVertical: 10,
    borderRadius: 25
  },
  active: {
    backgroundColor: '#FFFFFF',
  },
  inactive: {
    backgroundColor: '#FFFFFF',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});