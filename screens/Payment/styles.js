import styled from 'styled-components/native'


export const Container = styled.View`
    flex: 1;
    background-color: #F2F2F2;
    padding-bottom: 15;

`

export const TextPayment = styled.Text`
    font-size: 30;
    font-weight: bold;
    margin-left: 10px;
    padding-top: 30px;
    color: #fff
`

