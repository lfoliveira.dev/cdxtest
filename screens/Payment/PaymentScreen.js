import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, Image, TouchableOpacity } from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-ui'


import { Container, TextPayment } from './styles'
import menu from '../../assets/menu.png'
import CardsReal from '../ViewReal/CardsReal'
import CardCrypto from '../ViewCripto/CardCrypto'

export default class PaymentScreen extends Component {


  static navigationOptions = ({  }) => {
      return {
          header: null,
      }
  }  


  constructor(props) {
    super(props);
    this.state = {
        selectedIndex: 0,
        selectedIndices: [0],
        customStyleIndex: 0,
        blrForCripto: 'BLR para Cripto',

    };
  }

  handleSingleIndexSelect = (index) => {
    this.setState({
        selectedIndex: index
    });
  }

    handleStateSelect = () => {
    this.setState({
        blrForCripto: 'Cripto para BLR'
    });
    }

    handleCustomIndexSelect = (index) => {
        this.setState({
            customStyleIndex: index
        });
    }


    handleCustomIndexSelect = (index) => {
    this.setState(prevState => ({ ...prevState, customStyleIndex: index }))
    }

    handleSelectTitleBackground = () => {
        const { customStyleIndex } = this.state
        if (customStyleIndex === 0) {
          return {
            paddingTop: 50,
            margin: 0,
            backgroundColor: '#9f64ad',
            height: 170,
            flexDirection: 'row',
          
          }
        }
  
        else {
          return {
            paddingTop: 50,
            backgroundColor: '#6f64ad',
            height: 170,
            flexDirection: 'row',
           
          }
        }
      }

      handleSelectStateBackground = () => {
        const { customStyleIndex } = this.state
        if (customStyleIndex === 0) {
          return {
            margin: 0,
            backgroundColor: '#9f64ad',
            height: 60,
            alignItems: 'center',
            paddingTop: 20,
          }
        }
  
        else {
          return {
            backgroundColor: '#6f64ad',
            height: 60,
            alignItems: 'center',
            paddingTop: 20,
          }
        }
      }
  
  


  render() {
    const { customStyleIndex } = this.state
    return (
      <Container>
        <View style={this.handleSelectTitleBackground()}>
            <TextPayment>Pagamentos</TextPayment>
            <TouchableOpacity style={{ paddingTop: 35, marginLeft: 150, }}  onPress={() => this.props.navigation.navigate('Config')}>
            <Image source={menu} style={{ height: 20, width: 20, }} />
            </TouchableOpacity> 
        </View>

        <SegmentedControlTab
                values={['Reais', 'Cripto']}
                selectedIndex={this.state.customStyleIndex}
                onTabPress={this.handleCustomIndexSelect}
                borderRadius={5}
                tabsContainerStyle={{ borderWidth: 0, borderColor: 'transparent',  }}
                tabStyle={{ height: 45, paddingVertical: 0, borderLeftWidth: 0, borderLeftColor: '#000000' }}
                activeTabStyle={{ backgroundColor: '#FFF' }}
                tabTextStyle={{}}
                activeTabTextStyle={{ color: '#000000', fontSize: 16 }} 
            />
            <View style={this.handleSelectStateBackground()}>
            {customStyleIndex === 0
                && <Text style={{ color: '#FFF' }}>BLR para Cripto</Text>}
            {customStyleIndex === 1
                && <Text style={{ color: '#FFF' }}>Cripto para BLR</Text>}
            </View>

            {customStyleIndex === 0
                && <CardsReal />}
            {customStyleIndex === 1
                && <CardCrypto />}             



      </Container>

    );
  }
}
