import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text, View, Image, ScrollView, Animated, SafeAreaView, Dimensions, Alert } from 'react-native'
import {  Button, Icon, Fab } from 'native-base';

import { Container, TextScreen, ContainerNumbers, ContainerText } from './styles';
import {  SimpleLineIcons, MaterialCommunityIcons } from '@expo/vector-icons'

import BackScreen from '../../assets/back.png'
import rotate from '../../assets/rotate.png'
import switchoff  from '../../assets/switch.png'
import line from '../../assets/substract.png'
import soma from '../../assets/mathematics-01-512.png'
import left from '../../assets/left-arrow.png'
import user from '../../assets/user.png'
import correcto from '../../assets/correct.png'


import FabScreen from '../FAB/FabScreen'



export default class ConfigScreen extends Component {

  static navigationOptions = ({ navigation }) => ({
    headerStyle: {
      height: 180,
      backgroundColor: '#104E8B',
    },
    headerLeft: (
      <TouchableOpacity style={{ marginBottom: 100, marginLeft: 10 }} onPress={() => navigation.navigate('Pagar')}>
        <Image source={left} style={{ width: 40, height: 40,  }} />
      </TouchableOpacity>
    )

  });

  state = {
    y: new Animated.Value(0)
  }



  render() {
    const { y } = this.state
    return(
      <SafeAreaView style={styles.root}>
        <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-around' }}>
          <Text>ATARID</Text>
          <Text>123456789123456789</Text>
          <Image source={rotate} style={{ height: 20, width: 20,  }}
          />

        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-around' }}>
          <Text>Stratum</Text>
          <Text>123456789123456789</Text>
          <Image source={rotate} style={{ height: 20, width: 20,  }}
          />
        </View>

        <View>
        <View style={{ flexDirection: 'row', marginTop: 30, justifyContent: 'space-around', opacity: 0.5, }}>
          <Text>Habilitar compra rapida</Text>
          <Image source={switchoff} style={{ height: 40, width: 40, paddingBottom: 40 }}
          />
        </View>
        <View style={{ flexDirection: 'row', marginTop: 15, justifyContent: 'space-between', marginEnd: 53, marginStart: 54, opacity: 0.5, }}>
          <Text stye={{ marginTop: 60 }}>Modo Escuro</Text>
          <Image source={switchoff} style={{ height: 40, width: 40, paddingBottom: 40  }}
          />
        </View>
        </View>


        <View style={{ marginVertical: 15, marginTop: 2 }}>
        <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', marginEnd: 53, marginStart: 54 }}>
          <Image source={line} style={{ height: 2, width: 250, paddingBottom: 40  }}
          />
        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', marginEnd: 53, marginStart: 54 }}>
          <Text stye={{ marginTop: 50 }}>Carteiras</Text>
          <Image source={soma} style={{ height: 25, width: 35, marginRight: 25  }}
          />
        </View>
        </View>

        <ScrollView>
        <FabScreen />
        <View style={{ marginLeft: 50, paddingTop: 60 }}>
          <Text style={{ fontWeight: 'bold', color: '#FF0000' }}>Sair da conta</Text>
        </View>
        </ScrollView>

                
      </SafeAreaView>

    )
  }
}


const styles = StyleSheet.create({
  root: {
    flex: 1,
    marginBottom: 16,
    backgroundColor: '#F2F2F2'
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2'
  },
  container: {
    backgroundColor: '#f2f2f2',
    paddingTop: 20,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
    borderRadius: 20,
  },
  headerText: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#FF0000'
  },
  headerTextEfetuado: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#000000'
  },
  headerTextExpirado: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#000000'
  },
  content: {
    padding: 20,
    backgroundColor: '#fff',
    marginVertical: 10,
    borderRadius: 25
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },


});
