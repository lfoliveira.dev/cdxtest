import styled from 'styled-components/native'


export const Container = styled.View`
    flex: 1;
    background-color: #D3D3D3;
    
`

export const TextScreen = styled.Text`
    font-size: 16;

`

export const ContainerNumbers = styled.View`
    align-items: center;
    padding-top: 50px;
    margin-vertical: 20px;

`

