import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, TouchableOpacity, Image, Button } from 'react-native';
import Constants from 'expo-constants'
import * as Animatable from 'react-native-animatable'
import Collapsible from 'react-native-collapsible'
import Accordion from 'react-native-collapsible/Accordion'
import { Ionicons, FontAwesome } from '@expo/vector-icons'


import notification from '../../assets/exclamation.png'
import up from '../../assets/up-arrow.png'
import bitcoins from '../../assets/bitcoin.png'
import etherums from '../../assets/ethereum.png'
import zcore from '../../assets/zcore300.png'
 



const CONTENT = [
  {
    title: 'My Wallet BTC',
  },
];


export default class FabScreen extends Component {
  state = {
    activeSections: [],
    collapsed: true,
    multipleSelect: false,
  };

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections,
    });
  };

  renderHeader = (section, _, isActive) => {
    return (

      <Animatable.View
        duration={200}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
      <View style={{ flexDirection: 'row', }}>
      <Image source={bitcoins} style={{ height: 45, width: 45, }} />
        <Text style={styles.headerText}>{section.title} 01</Text>
      </View>
      <Text style={{ marginLeft: 10 }}>BTC</Text>
      <View style={{ marginLeft: 55,  }}>
        <Text style={{ }}>Endereço:</Text>
        <Text style={{  }}>asldjahaifhlaDOJO3294873HDFKA</Text>
      </View>

      </Animatable.View>

    );
  };


  renderContent(section, _, isActive) {
    return (
      <View>
      <Animatable.View
        duration={400}
        style={[styles.content, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor">
      <View style={{ flexDirection: 'row', }}>
      <Image source={bitcoins} style={{ height: 45, width: 45, }} />
        <Text style={styles.headerText}>{section.title} 02</Text>
      </View>
      <Text style={{ marginLeft: 10 }}>BTC</Text>
      <View style={{  marginLeft: 55, }}>
      <Text style={{  }}>Endereço:</Text>
      <Text style={{ }}>qdjosahdiadp13ejld12312321asldja</Text>

        
      </View>

      </Animatable.View>


      <Animatable.View
      duration={400}
      style={[styles.content, isActive ? styles.active : styles.inactive]}
      transition="backgroundColor">
      <View style={{ flexDirection: 'row', }}>
      <Image source={etherums} style={{ height: 45, width: 45, }} />
      <Text style={styles.headerText}>MyWallet ETH 01</Text>
      </View>
      <Text style={{ marginLeft: 10 }}>ETH</Text>
      <View style={{ marginLeft: 55, }}>
      <Text style={{  }}>Endereço:</Text>
      <Text style={{  }}>sldhaishdao9eu321093812qofjdjsdjasd</Text>


      </View>
      </Animatable.View>


      <Animatable.View
      duration={400}
      style={[styles.content, isActive ? styles.active : styles.inactive]}
      transition="backgroundColor">
      <View style={{ flexDirection: 'row', }}>
      <Image source={zcore} style={{ height: 45, width: 45, }} />
      <Text style={styles.headerText}>MyWallet ZCR 01</Text>
      </View>
      <Text style={{ marginLeft: 10 }}>ZCR</Text>
      <View style={{ marginLeft: 55, paddingTop: 5 }}>
      <Text style={{ }}>Endereço:</Text>
      <Text style={{  }}>aaAAAdkjfakljl0928391802948lksajfsldl</Text>

      </View>
      </Animatable.View>
      </View>

    );
  }
  

  render() {
    const { multipleSelect, activeSections } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{  }}>
          <Accordion
            activeSections={activeSections}
            sections={CONTENT}
            touchableComponent={TouchableOpacity}
            expandMultiple={multipleSelect}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F2F2F2',
    paddingTop: Constants.statusBarHeight,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
    borderRadius: 20,
  },
  headerText: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#bbb7b7'
  },
  content: {
    padding: 20,
    backgroundColor: '#fff',
    marginVertical: 20,
  },
  active: {
    backgroundColor: 'rgba(255,255,255,1)',
  },
  inactive: {
    backgroundColor: 'rgba(245,252,255,1)',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});