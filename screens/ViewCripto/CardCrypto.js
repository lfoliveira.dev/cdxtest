import React, { Component } from 'react';
import { Switch, ScrollView, StyleSheet, Text, View, TouchableOpacity, Image, Button } from 'react-native';
import Constants from 'expo-constants'
import * as Animatable from 'react-native-animatable'
import Collapsible from 'react-native-collapsible'
import Accordion from 'react-native-collapsible/Accordion'
import { Ionicons, FontAwesome } from '@expo/vector-icons'


import notification from '../../assets/exclamation.png'
import up from '../../assets/up-arrow.png'



const CONTENT = [
  {
    title: 'Pendente',
  
  },
];


export default class CardsCripto extends Component {
  state = {
    activeSections: [],
    collapsed: true,
    multipleSelect: false,
  };

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  setSections = sections => {
    this.setState({
      activeSections: sections.includes(undefined) ? [] : sections,
    });
  };

  renderHeader = (section, _, isActive) => {
    return (

      <Animatable.View
        duration={200}
        style={[styles.header, isActive ? styles.active : styles.inactive]}
        transition="backgroundColor"
      >
      <View style={{ flexDirection: 'row', }}>
      <Image source={notification} style={{ height: 25, width: 25, }} />
        <Text style={styles.headerText}>{section.title}</Text>
        <Text style={{ marginLeft: 70 }}>06 MAI 2019</Text>
        <Text style={{ marginLeft: 10 }}>14:25</Text>
      </View>
      <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
        <Text style={{ paddingTop: 5 }}>Cripto a enviar:</Text>
        <Animatable.Text 
        animation={isActive ? 'bounceIn' : undefined}
        style={{ marginTop: 5, marginLeft: 95, fontWeight: 'bold', fontSize: 15 }}
        >
        1.0000 BTC</Animatable.Text>
        
      </View>
      <View style={{ flexDirection: 'row', marginLeft: 35, paddingTop: 5 }}>
        <Text style={{ paddingTop: 5 }}>Valor a receber:</Text>
        <Text style={{ marginTop: 5, marginLeft: 85 }}> R$ 22.539,22</Text>
      </View>
      </Animatable.View>

    );
  };


  renderContent(section, _, isActive) {
    return (
      <View />
    );
  }
  

  render() {
    const { multipleSelect, activeSections } = this.state;

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{  }}>
          <Accordion
            activeSections={activeSections}
            sections={CONTENT}
            touchableComponent={TouchableOpacity}
            expandMultiple={multipleSelect}
            renderHeader={this.renderHeader}
            renderContent={this.renderContent}
            duration={400}
            onChange={this.setSections}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F2F2F2',
    paddingTop: Constants.statusBarHeight,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
    borderRadius: 20,
  },
  headerText: {
    marginLeft: 10,
    textAlign: 'left',
    fontSize: 16,
    fontWeight: '500',
    color: '#FF0000'
  },
  content: {
    padding: 20,
    backgroundColor: '#FFFFFF',
    marginVertical: 20,
  },
  active: {
    backgroundColor: '#FFFFFF',
  },
  inactive: {
    backgroundColor: '#FFFFFF',
  },
  selectors: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selector: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  activeSelector: {
    fontWeight: 'bold',
  },
  selectTitle: {
    fontSize: 14,
    fontWeight: '500',
    padding: 10,
  },
  multipleToggle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 30,
    alignItems: 'center',
  },
  multipleToggle__title: {
    fontSize: 16,
    marginRight: 8,
  },
});