import React from 'react';
import { Platform, View, Text, TouchableOpacity, Image } from 'react-native';
import { createStackNavigator, createSwitchNavigator, } from 'react-navigation';
import { Ionicons, EvilIcons } from '@expo/vector-icons'

import ConfigScreen from '../screens/Configuration/ConfigScreen'
import user from '../assets/user.png'


const Config = createStackNavigator({
  Config: ConfigScreen,
},
  {
    defaultNavigationOptions: {
      headerTitle: 
      <View>
      <Text style={{ fontSize: 30, color: '#fff', fontWeight: 'bold', marginRight: 25 }}>
      Configurações
      </Text>
      <View style={{ marginTop: 20 }}>
      <View style={{ alignItems: 'center', }}>
      <Image
      source={user}
      style={{ height: 40, width: 40 }}
      />
      </View>
      <View style={{ alignItems: 'center',}}>
      <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>Walter</Text>
      <Text style={{ color: '#fff' }}>Email: walter@gmail.com</Text>
      </View>
      </View>
      </View>
      , 
      headerTintColor: '#000',
    },
    
    
  });


export default createSwitchNavigator({
  Config,
});