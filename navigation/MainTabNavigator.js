import React from 'react';
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons'

import CreateOrderScreen from '../screens/CreateOrder/CreateOrderScreen'
import FavoriteScreen from '../screens/Favorite/FavoriteScreen'
import MarketScreen from '../screens/Market/MarketScreen'
import MyOrderScreen from '../screens/MyOrder/MyOrderScreen'
import PaymentScreen from '../screens/Payment/PaymentScreen'
import ConfigScreen from '../screens/Configuration/ConfigScreen'




const Mercado = createStackNavigator({
  screen: MarketScreen
})


const MinhasOrdens = createStackNavigator({
  screen: MyOrderScreen
})

const CriarOrdem = createStackNavigator({
  screen: CreateOrderScreen
})

const Pagar = createStackNavigator({
  screen: PaymentScreen
})

const Favoritos = createStackNavigator({
  screen: FavoriteScreen
})

const Config = createStackNavigator({
  screen: ConfigScreen
})


const TabNavigation = createBottomTabNavigator({
  Mercado: { 
    screen: Mercado,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
          <MaterialCommunityIcons
              name="chart-line-stacked"
              color={tintColor}
              size={24}
          />
      ),
      
  })
  
  },
  MinhasOrdens: { 
      screen: MinhasOrdens,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
            <Ionicons
                name="ios-paper"
                color={tintColor}
                size={24}
            />
        )
    })
  
  },
  CriarOrdem: { 
      screen: CriarOrdem,
      navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
            <Ionicons
                name="ios-add-circle-outline"
                color={tintColor}
                size={24}
            />
        )
    })
  
  },
  Pagar: {
    screen: Pagar,
    navigationOptions: () => ({
      tabBarIcon: ({tintColor}) => (
          <MaterialIcons
          name="attach-money"
          color={tintColor}
          size={24}
          />
      ),
      
  })
  
  
  
  },


  Favoritos: {
    screen: Favoritos,
    navigationOptions: () => ({
        tabBarIcon: ({tintColor}) => (
            <MaterialIcons
                name="star-border"
                color={tintColor}
                size={24}
            />
        )
    })
  
  },
},
{
  initialRouteName: "Pagar",
  tabBarOptions: {
    showIcon: true,
    activeTintColor: '#ffd9aa'

  }
  

}

)


export default createAppContainer(TabNavigation, Config);