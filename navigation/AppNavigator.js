import { createAppContainer, createSwitchNavigator, createDrawerNavigator } from 'react-navigation';

import TabNavigation from './MainTabNavigator'
import Config from './DrawerNavigation'


export default createAppContainer(
  createSwitchNavigator({
    Main: TabNavigation,
    Second: Config
  },
 )
)